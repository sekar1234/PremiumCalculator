package com.emids.controller;

import com.emids.UserModel.PremiumModel;

public class PremiumCalculationdefault implements CalculatePremium {
	public double calculatePremium(PremiumModel person) {
		
		double premium = 5000;
		premium = Condition.applyAgeBasedCalcs(person, premium);
		premium = Condition.applyGenderBasedCalcs(person, premium);
		premium = Condition.applyHealthIssuesBasedCalcs(person, premium);
		premium = Condition.applyGoodHabbitsBasedCalcs(person, premium);
		premium = Condition.applyBadHabbitsBasedCalcs(person, premium);
		return premium;
	}

}
