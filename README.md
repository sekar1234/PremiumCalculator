package com.emids.UserModel;

import java.util.HashSet;
import java.util.Set;

public class PremiumModel {
	private String name;
	private Gender gender;
	private int age;
	private Set<HealthProblem> healthproblem;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Set<HealthProblem> getHealthproblem() {
		if (healthproblem == null) {
			healthproblem = new HashSet<HealthProblem>();
		}

		return healthproblem;
	}
	public void setHealthproblem(Set<HealthProblem> healthproblem) {
		this.healthproblem = healthproblem;
	}
	public Set<Habbits> getHabbits() {
		if (habbits == null) {
			habbits = new HashSet<Habbits>();
		}

		return habbits;
	}
	public void setHabbits(Set<Habbits> habbits) {
		this.habbits = habbits;
	}
	private Set<Habbits> habbits;
}