package com.emids.controller;

import java.util.HashSet;
import java.util.Set;

import com.emids.UserModel.Habbits;

public interface Constants {

	Set<Habbits> goodHabbits = new HashSet<Habbits>() {
		{
			add(Habbits.DAILYEXCERCISE);
		}
	};

	Set<Habbits> badHabbits = new HashSet<Habbits>() {
		{
			add(Habbits.SOMOKING);
			add(Habbits.ALCHOHOL);
			add(Habbits.DRUGS);
		}
	};

}
