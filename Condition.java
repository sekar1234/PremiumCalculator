package com.emids.controller;

import com.emids.UserModel.Gender;
import com.emids.UserModel.Habbits;
import com.emids.UserModel.HealthProblem;
import com.emids.UserModel.PremiumModel;

public class Condition {
	public static double applyBadHabbitsBasedCalcs(PremiumModel person, double premium) {
	
		for (Habbits habbit : person.getHabbits()) {
			if (Constants.badHabbits.contains(habbit)) {
				premium *= 1.03;
			}
		}
		return premium;
	}

	public static double applyGoodHabbitsBasedCalcs(PremiumModel person, double premium) {
		for (Habbits habbit : person.getHabbits()) {
			if (Constants.goodHabbits.contains(habbit)) {
				premium *= 0.97;
			}
		}
		return premium;
	}

	public static double applyHealthIssuesBasedCalcs(PremiumModel person, double premium) {
		for (HealthProblem issue : person.getHealthproblem()) {
			premium *= 1.01;
		}
		return premium;
	}

	public static double applyGenderBasedCalcs(PremiumModel person, double premium) {
		// Add gender specific adjustments
		if (person.getGender() == Gender.MALE) {
			premium *= 1.02;
		}
		return premium;
	}

	public static double applyAgeBasedCalcs(PremiumModel person, double premium) {
	
		if (person.getAge() >= 18) {
			premium *= 1.1;
		}
		if (person.getAge() >= 25) {
			premium *= 1.1;
		}
		if (person.getAge() >= 30) {
			premium *= 1.1;
		}
		if (person.getAge() >= 35) {
			premium *= 1.1;
		}
		if (person.getAge() >= 40) {
		
			int age = person.getAge() - 40;
			while (age >= 5) {
				premium *= 1.2;
				age -= 5;
			}
		}
		return premium;
	}
}
