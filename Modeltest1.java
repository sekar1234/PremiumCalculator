package com.emids.java.test;

import static org.junit.Assert.*;

import java.util.Set;

import org.hamcrest.Matcher;
import org.junit.Test;

import com.emids.UserModel.Gender;
import com.emids.UserModel.Habbits;
import com.emids.UserModel.HealthProblem;
import com.emids.UserModel.PremiumModel;
import com.emids.controller.PremiumCalculationdefault;

public class Modeltest1 {

	private PremiumCalculationdefault strategy = new PremiumCalculationdefault();

	@Test
	public void testCalculatePremium_NormanGomes() throws Exception {
		PremiumModel person = new PremiumModel();
		person.setName("Norman Gomes");
		person.setGender(Gender.MALE);
		person.setAge(34);
		Set<HealthProblem> issues = person.getHealthproblem();
		issues.add(HealthProblem.OVERWEIGHT);
		Set<Habbits> habbits = person.getHabbits();
		habbits.add(Habbits.ALCHOHOL);
		habbits.add(Habbits.DAILYEXCERCISE);

		double premium = strategy.calculatePremium(person);

		assertThat(premium, closeTo(6856d, 10d));
	}

	private Matcher closeTo(double d, double e) {
		// TODO Auto-generated method stub
		return null;
	}


}
