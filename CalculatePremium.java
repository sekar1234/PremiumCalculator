package com.emids.controller;

import com.emids.UserModel.PremiumModel;

public interface CalculatePremium {

	double calculatePremium(PremiumModel person);

}
